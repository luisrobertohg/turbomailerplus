<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ['paise_id' => 1, 'estado' => 'Texas'],
            ['paise_id' => 1, 'estado' => 'Nuevo México'],
            ['paise_id' => 2, 'estado' => 'Quintana Roo'],
            ['paise_id' => 2, 'estado' => 'Veracruz']
        ];
        DB::table('estados')->insert($data);
    }
}
