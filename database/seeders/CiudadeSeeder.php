<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CiudadeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ['estado_id' => '1', 'ciudade' => 'Dallas'],
            ['estado_id' => '1', 'ciudade' => 'Houston'],
            ['estado_id' => '2', 'ciudade' => 'Alburqueque'],
            ['estado_id' => '2', 'ciudade' => 'Roswell'],
            ['estado_id' => '3', 'ciudade' => 'Chetumal'],
            ['estado_id' => '3', 'ciudade' => 'Cancún'],
            ['estado_id' => '4', 'ciudade' => 'Minatitlán'],
            ['estado_id' => '4', 'ciudade' => 'Coatzacoalcos']
        ];
        DB::table('ciudades')->insert($data);
        
    }
}
