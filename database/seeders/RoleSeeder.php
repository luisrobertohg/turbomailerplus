<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //Generamos Roles de admin y usuario
        $admin = Role::create(['name' => 'admin']);

        $permission = Permission::create(['name' => 'ver-usuario'])->syncRoles([$admin]);
        $permission = Permission::create(['name' => 'crear-usuario'])->syncRoles([$admin]);
        $permission = Permission::create(['name' => 'editar-usuario'])->syncRoles([$admin]);
        $permission = Permission::create(['name' => 'borrar-usuario'])->syncRoles([$admin]);
    }
}
