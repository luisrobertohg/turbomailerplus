<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        //Ejecutamos el seeder para generar los roles
        $this->call(RoleSeeder::class);

        //Insertamos usuario admin por seeder
        User::create([
            'name' => 'superadmin',
            'email' => 'superadmin@correo.com',
            'password' => Hash::make('12345678'),
        ])->assignRole('admin');
        
        //Ejecutamos seeder de paises
        $this->call(PaiseSeeder::class);
        //Ejecutamos seeder de estados
        $this->call(EstadoSeeder::class);
        //Ejecutamos seeder de ciudades
        $this->call(CiudadeSeeder::class);
    }
}
