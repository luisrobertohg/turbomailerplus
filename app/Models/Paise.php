<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Paise
 *
 * @property $id
 * @property $paise
 * @property $created_at
 * @property $updated_at
 *
 * @property Estado[] $estados
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Paise extends Model
{
    
    static $rules = [
		'paise' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['paise'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function estados()
    {
        return $this->hasMany('App\Models\Estado', 'paise_id', 'id');
    }
    

}
