<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\EnviarCorreo;

class EmailController extends Controller
{
    public function enviarcorreo(Request $request){
        $asunto = $request->input('asunto');
        $email = $request->input('email');
        $contenido = $request->input('mensaje');

        Mail::to($email)->queue(new EnviarCorreo($asunto,$contenido));
        return redirect('home')->with('status', 'Correo enviado');
    }
}
