<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Paise;
use App\Models\Estado;
use App\Models\Ciudade;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rules\Password;

class UsuarioController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:ver-usuario|crear-usuario|editar-usuario|borrar-usuario', ['only' => ['index']]);
        $this->middleware('permission:crear-usuario', ['only' => ['create', 'store']]);
        $this->middleware('permission:editar-usuario', ['only' => ['edit', 'update']]);
        $this->middleware('permission:borrar-usuario', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $usuarios = User::all();
        return view('usuarios.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $paises = Paise::pluck('paise', 'id');
        return view('usuarios.crear', compact('paises'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->fecha_nac);
        $this->validate($request, [
            'name' => 'required|max:100',
            'email' => 'required|email|unique:users,email',
            'fecha_nac' => 'required|before_or_equal:' . \Carbon\Carbon::now()->subYears(18)->format('Y-m-d'),
            'password' => ['required', 'same:confirm-password', Password::min(8)->mixedCase()->numbers()->symbols()],
            'telefono' => 'max:10',
            'cedula' => 'required|max:11',
            'ciudade_id' => 'required'
        ], [
            'fecha_nac.before_or_equal' => 'El usuario debe ser mayor de 18 años',
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);

        return redirect()->route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $paises = Paise::pluck('paise', 'id');

        return view('usuarios.editar', compact('user', 'paises'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!empty($request->password)) {
            $this->validate($request, [
                'name' => 'required|max:100',
                'fecha_nac' => 'required|before_or_equal:' . \Carbon\Carbon::now()->subYears(18)->format('Y-m-d'),
                'password' => ['required', 'same:confirm-password', Password::min(8)->mixedCase()->numbers()->symbols()],
                'telefono' => 'max:10',
                'ciudade_id' => 'required'
            ], [
                'fecha_nac.before_or_equal' => 'El usuario debe ser mayor de 18 años',
            ]);
        } else {
            $this->validate($request, [
                'name' => 'required|max:100',
                'fecha_nac' => 'required|before_or_equal:' . \Carbon\Carbon::now()->subYears(18)->format('Y-m-d'),
                'telefono' => 'max:10',
                'ciudade_id' => 'required'
            ], [
                'fecha_nac.before_or_equal' => 'El usuario debe ser mayor de 18 años',
            ]);
        }

        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }

        $user = User::find($id);
        $user->update($input);

        return redirect()->route('usuarios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('usuarios.index');
    }

    public function obtener_estados(Request $request)
    {
        $estados = Estado::where('paise_id', $request->paise_id)->get();
        return response()->json($estados);
    }
    public function obtener_ciudades(Request $request)
    {
        $ciudades = Ciudade::where('estado_id', $request->estado_id)->get();
        return response()->json($ciudades);
    }
}
