<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaiseController;
use App\Http\Controllers\EstadoController;
use App\Http\Controllers\CiudadeController;
use App\Http\Controllers\UsuarioController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(["register" => false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Creamos un grupo de rutas protegidas
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', function () {
        return view('home');
    });
    Route::resource('usuarios', UsuarioController::class);
    Route::resource('paises', PaiseController::class);
    Route::resource('estados', EstadoController::class);
    Route::resource('ciudades', CiudadeController::class);

    Route::get('obtener_estados', [UsuarioController::class, 'obtener_estados'])->name('obtener.estados');
    Route::get('obtener_ciudades', [UsuarioController::class, 'obtener_ciudades'])->name('obtener.ciudades');

    Route::get('/crearcorreo', function () {
        return view('correo.formulario');
    })->name('crearcorreo');
    
    Route::post('/enviarcorreo', [App\Http\Controllers\EmailController::class, 'enviarcorreo'])->name('enviarcorreo');


});

