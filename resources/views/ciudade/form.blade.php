<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('estado_id') }}
            {{ Form::text('estado_id', $ciudade->estado_id, ['class' => 'form-control' . ($errors->has('estado_id') ? ' is-invalid' : ''), 'placeholder' => 'Estado Id']) }}
            {!! $errors->first('estado_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('ciudade') }}
            {{ Form::text('ciudade', $ciudade->ciudade, ['class' => 'form-control' . ($errors->has('ciudade') ? ' is-invalid' : ''), 'placeholder' => 'Ciudade']) }}
            {!! $errors->first('ciudade', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
    </div>
</div>