<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('paise') }}
            {{ Form::text('paise', $paise->paise, ['class' => 'form-control' . ($errors->has('paise') ? ' is-invalid' : ''), 'placeholder' => 'Paise']) }}
            {!! $errors->first('paise', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
    </div>
</div>