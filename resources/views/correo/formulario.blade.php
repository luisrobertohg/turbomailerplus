@include('layouts.app')
<div class="container">
    <form action="{{route('enviarcorreo')}}" method="POST">
        @csrf
        <div class="mb-3">
            <label for="asunto" class="form-label">Asunto:</label>
            <input type="text" name="asunto" class="form-control" id="asunto">
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Destinatario:</label>
            <input type="email" name="email" class="form-control" id="email">
        </div>
          <div class="mb-3">
            <label for="mensaje" class="form-label">Mensaje</label>
            <textarea class="form-control" name="mensaje" id="mensaje" rows="3"></textarea>
          </div>
          <div class="mb-3">
                <button type="submit" class="btn btn-primary">Enviar</button>
          </div>
    </form>
    
</div>