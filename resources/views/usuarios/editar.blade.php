@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading">Editar Usuario</h3>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            @if ($errors->any())
                                <div class="alert alert-dark alert-dismissible fade show" role="alert">
                                    <strong>¡Revise los campos!</strong>
                                    @foreach ($errors->all() as $error)
                                        <span class="badge badge-danger">{{ $error }}</span>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <p><b>E-mail:</b> {{ $user->email }}</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <p><b>Cédula:</b> {{ $user->cedula }}</p>
                                    </div>
                                </div>
                            </div>

                            {!! Form::model($user, ['method' => 'PATCH', 'route' => ['usuarios.update', $user->id]]) !!}
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="name">Nombre</label>
                                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        {!! Form::password('password', ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="confirm-password">Confirmar Password</label>
                                        {!! Form::password('confirm-password', ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="telefono">Número Celular</label>
                                        {!! Form::number('telefono', null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="fecha_nac">Fecha Nacimiento</label>
                                        {!! Form::date('fecha_nac', null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="paise_id">Pais</label>
                                        {{ Form::select('paise_id', $paises, null, ['id' => 'paise_id', 'class' => 'form-control', 'placeholder' => 'Selecciona un país']) }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="estado_id">Estado</label>
                                        {{ Form::select('estado_id', [], null, ['id' => 'estado_id', 'class' => 'form-control', 'placeholder' => 'Selecciona un estado']) }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="ciudade_id">Ciudad</label>
                                        {{ Form::select('ciudade_id', [$user->ciudade->id => $user->ciudade->ciudade], null, ['id' => 'ciudade_id', 'class' => 'form-control', 'placeholder' => 'Selecciona una ciudad']) }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            //asignamos los select a variables
            var selectPaises = $('#paise_id');
            var selectEstados = $('#estado_id');
            var selectCiudades = $('#ciudade_id');

            //funcion para llenar select de estados cuando se cambie de opcion en el select de paises
            selectPaises.change(function() {
                var paise_id = $(this).val();
                selectEstados.empty();
                selectCiudades.empty();
                $.ajax({
                    url: "{{ route('obtener.estados') }}",
                    type: 'get',
                    data: {
                        paise_id: paise_id
                    },
                    dataType: 'json',
                    success: function(response) {
                        selectEstados.append('<option value="">Selecciona un estado</option>')
                        selectCiudades.append('<option value="">Selecciona una ciudad</option>')
                        $.each(response, function(key) {
                            selectEstados.append("<option value='" + response[key].id +
                                "'>" + response[key].estado + "</option>");
                        });
                    },
                    error: function() {
                        alert(
                            'Hubo un error obteniendo los estados. Contacte al administrador.'
                        );
                    }
                });
            });

            //funcion para llenar select de estados cuando se cambie de opcion en el select de paises
            selectEstados.change(function() {
                var estado_id = $(this).val();
                selectCiudades.empty();
                $.ajax({
                    url: "{{ route('obtener.ciudades') }}",
                    type: 'get',
                    data: {
                        estado_id: estado_id
                    },
                    dataType: 'json',
                    success: function(response) {
                        console.log(response);
                        selectCiudades.append('<option value="">Selecciona una ciudad</option>')
                        $.each(response, function(key) {
                            selectCiudades.append("<option value='" + response[key].id +
                                "'>" + response[key].ciudade + "</option>");
                        });
                    },
                    error: function() {
                        alert(
                            'Hubo un error obteniendo los estados. Contacte al administrador.'
                        );
                    }
                });
            });

        });
    </script>
@endsection
