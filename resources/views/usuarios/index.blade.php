@extends('layouts.app')
@section('template_title')
    Usuarios
@endsection
@section('css')
    <link rel="stylesheet" href="http://cdn.datatables.net/1.13.4/css/jquery.dataTables.css" />
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Listado de usuarios') }}
                            </span>

                            <div class="float-right">
                                <a class="btn btn-primary btn-sm" href="{{ route('usuarios.create') }}">Nuevo usuario</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="tabla_usuarios" class="table table-striped table-hover table-bordered table-sm w-100"
                                id="tabla_usuarios" style="white-space: nowrap;">
                                <thead>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>E-mail</th>
                                    <th>Teléfono</th>
                                    <th>Cédula</th>
                                    <th>Edad</th>
                                    <th>Ciudad</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                    @foreach ($usuarios as $usuario)
                                        <tr>
                                            <td>{{ $usuario->id }}</td>
                                            <td>{{ $usuario->name }}</td>
                                            <td>{{ $usuario->email }}</td>
                                            <td>{{ $usuario->telefono }}</td>
                                            <td>{{ $usuario->cedula }}</td>
                                            <td>
                                                @php
                                                    $nacimiento = new DateTime($usuario->fecha_nac);
                                                    $ahora = new DateTime(date('Y-m-d'));
                                                    $diferencia = $ahora->diff($nacimiento);
                                                    echo $diferencia->format('%y');
                                                @endphp
                                            </td>
                                            <td>{{ $usuario->ciudade->ciudade }}</td>
                                            <td>
                                                <a class="btn btn-info btn-sm"
                                                    href="{{ route('usuarios.edit', $usuario->id) }}">Editar</a>

                                                {!! Form::open([
                                                    'method' => 'DELETE',
                                                    'route' => ['usuarios.destroy', $usuario->id],
                                                    'style' => 'display:inline',
                                                    'class' => 'eliminar-usuario',
                                                    'data-id' => $usuario->email,
                                                ]) !!}
                                                {!! Form::submit('Borrar', ['class' => 'btn btn-danger btn-sm']) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {
            $('#tabla_usuarios').DataTable();
        });
    </script>
@endsection
